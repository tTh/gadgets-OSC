/*
 *
 *  code semi-grabed from the original asedump.
 *
 */ 
 
#include  <stdio.h>
#include  <stdlib.h>

#include <alsa/asoundlib.h>

#include  "alsaseq.h"

extern int	verbosity;

/* ----------------------------------------------------------------- */
snd_seq_t * create_sequencer(char *name)
{
snd_seq_t 	*p_seq;
int		err;

#if DEBUG_LEVEL
fprintf(stderr, "---> %s ( '%s' )\n", __func__, name);
#endif

/* open sequencer */
err = snd_seq_open(&p_seq, "default", SND_SEQ_OPEN_DUPLEX, 0);
// fprintf(stderr, " seq_open --> %d\n", err);
if (err) {
	fprintf(stderr, "%s : open sequencer -% d", __func__, err);
	exit(1);
	}

/* set our client's name */
err = snd_seq_set_client_name(p_seq, name);
if (err) {
	fprintf(stderr, "%s : set client name -% d", __func__, err);
	exit(1);
	}

return p_seq;
}
/* ----------------------------------------------------------------- */
/* ----------------------------------------------------------------- */
int create_port(snd_seq_t *seq)
{
int err;

#if DEBUG_LEVEL
fprintf(stderr, "---> %s (%p)\n", __func__, seq);
#endif

err = snd_seq_create_simple_port(seq, "kontrol2osc",
			 SND_SEQ_PORT_CAP_WRITE 	|
			 SND_SEQ_PORT_CAP_SUBS_WRITE	,
			 SND_SEQ_PORT_TYPE_MIDI_GENERIC |
			 SND_SEQ_PORT_TYPE_APPLICATION	);
if (err) {			 
	fprintf(stderr, "%s:%s snd_seq_create_simple_port : err %d\n",
		__FILE__, __func__, err);
	}

return err;
}
/* ----------------------------------------------------------------- */

void list_ports(snd_seq_t *seq)
{
snd_seq_client_info_t	*cinfo;
snd_seq_port_info_t	*pinfo;
int			client;

snd_seq_client_info_alloca(&cinfo);
snd_seq_port_info_alloca(&pinfo);

puts(" Port    Client name                      Port name");

snd_seq_client_info_set_client(cinfo, -1);

while (snd_seq_query_next_client(seq, cinfo) >= 0) {
	client = snd_seq_client_info_get_client(cinfo);

	snd_seq_port_info_set_client(pinfo, client);
	snd_seq_port_info_set_port(pinfo, -1);
	while (snd_seq_query_next_port(seq, pinfo) >= 0) {
		/* we need both READ and SUBS_READ */
		if ((snd_seq_port_info_get_capability(pinfo)
		     & (SND_SEQ_PORT_CAP_READ | SND_SEQ_PORT_CAP_SUBS_READ))
		    != (SND_SEQ_PORT_CAP_READ | SND_SEQ_PORT_CAP_SUBS_READ))
			continue;
		printf("%3d:%-3d  %-32.32s %s\n",
		       snd_seq_port_info_get_client(pinfo),
		       snd_seq_port_info_get_port(pinfo),
		       snd_seq_client_info_get_name(cinfo),
		       snd_seq_port_info_get_name(pinfo));
		}
	}
}
/* ----------------------------------------------------------------- */
/* ----------------------------------------------------------------- */
/* ----------------------------------------------------------------- */
