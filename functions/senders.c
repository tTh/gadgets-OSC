/*
 * some functions for sending various data over the wire
 */
 
#include  <stdio.h>
#include  <lo/lo.h>

#include  "senders.h"

extern int verbosity;			/* not so ugly hack */

/* ----------------------------------------------------------------- */
int	send_data_xy(lo_address dst, int x, int y)
{
int		foo;

if (verbosity) fprintf(stderr, "sending x y    %7d  %7d\n", x, y);
foo = lo_send(dst, "/joystick/xy", "ii", x, y);
if (verbosity > 2) {
	fprintf(stderr, "lo_send -> %d\n", foo);
	}
return foo;
}
/* ----------------------------------------------------------------- */
/* function added Sun Mar 31 18:51:46 UTC 2024 */
int	send_data_zw(lo_address dst, int w, int z)
{
int		foo;

if (verbosity) fprintf(stderr, "sending w z    %7d  %7d\n", w, z);
foo = lo_send(dst, "/joystick/wz", "ii", w, z);
if (verbosity > 2) {
	fprintf(stderr, "lo_send -> %d\n", foo);
	}
return foo;
}
/* ----------------------------------------------------------------- */
int	send_data_button(lo_address dst, int n, int v)
{
int		foo;

if (verbosity) fprintf(stderr, "sending button %d %d\n", n, v);
foo = lo_send(dst, "/joystick/b", "ii", n, v);
if (verbosity > 2) {
	fprintf(stderr, "lo_send -> %d\n", foo);
	}
return foo;
}
/* ----------------------------------------------------------------- */
int	send_data_id(lo_address dst, char *s)
{
int		foo;

if (verbosity) fprintf(stderr, "sending my id '%s'\n", s);
foo = lo_send(dst, "/joystick/id", "s", s);
if (verbosity > 2) {
	fprintf(stderr, "lo_send -> %d\n", foo);
	}
return foo;
}
/* ----------------------------------------------------------------- */
