/*
 *   alsaseq.h
 */

/* ----------------------------------------------------------------- */

snd_seq_t * create_sequencer(char *name);

int create_port(snd_seq_t *seq);

void list_ports(snd_seq_t *seq);

/* ----------------------------------------------------------------- */
