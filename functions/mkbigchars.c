/*
 *
 */

#include   <stdio.h>
#include   <stdlib.h>
#include   <unistd.h>
#include   <fcntl.h>

/* ----------------------------------------------------------------- */
int make_bigfont_def(char *input, char *deffile)
{
FILE		*fp;
int		fd, idx, foo;
unsigned char	buffer[2048];

fprintf(stderr, ">>> %s ( '%s'  '%s' )\n", __func__, input, deffile);

/*
 *	load the binary font datas
 */
fd = open(input,  O_RDONLY, 0);
if (-1 == fd) {
	perror(input);
	exit(1);
	}
foo = read(fd, buffer, 2048);
if (2048 != foo) {
	fprintf(stderr, "%s: read %d bytes, 2048 expected\n", __func__, foo);
	close(fd);
	return 1;
	}
close(fd);

/*
 *	write the definition file as a .h c-file
 */
fp = fopen(deffile, "w");
if (NULL == fp) {
	perror(deffile);
	exit(1);
	}

fprintf(fp, "/*\n *   ! ! ! GENERATED FILE ! ! !\n*/\n");

for (idx=0; idx-256; idx++) {
	fprintf(fp, "/*\n *\t***\t%3d   0x%02x\n */\n", idx, idx);

	fprintf(fp, "\n");
	}

fprintf(fp, "/* yolo? */\n");

fclose(fp);

return 0;
}
/* ----------------------------------------------------------------- */
int main(int argc, char *argv[])
{
int		foo;

if (3 != argc) {
	fprintf(stderr, "ERR: %s need two filenames\n", argv[0]);
	exit(1);
	}

foo = make_bigfont_def(argv[1], argv[2]);

if (foo) {
	fprintf(stderr, "got %d from font converter.\n", foo);
	exit(1);
	}

return 0;
}
/* ----------------------------------------------------------------- */
