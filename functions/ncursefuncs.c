/*
 *	ncurses widgets for poc-osc
 */
 
#include  <stdio.h>
#include  <stdlib.h>
#include  <curses.h>
#include  <signal.h>
#include  <locale.h>

#include  "ncursefuncs.h"

extern int verbosity;		/* to be declared public near main() */

/* ----------------------------------------------------------------- */
void endcurses(int p)
{
endwin();
fprintf(stderr, "%s : p=%d, bye...\n", __func__, p);
exit(0);
}
static void atexit_endcurses(void)
{
#if DEBUG_LEVEL
fprintf(stderr, "--> %s ( )\n", __func__);
#endif
endcurses(0);
}
/* ----------------------------------------------------------------- */
int initcurses(void)
{
setlocale(LC_ALL, "");			/* pourquoi ? */

/* get character-at-a-time input without echoing */
initscr();     cbreak();     noecho();

/* hide the cursor */
curs_set(0);

atexit(atexit_endcurses);
// signal(SIGINT, endcurses);

return 0;		/* this is not a magic number */
}
/* ----------------------------------------------------------------- */
int draw_main_screen(char *title, int unused)
{
char		ligne[81];
int		foo;

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( '%s'  %d )\n", __func__, title, unused);
#endif 

if (verbosity)	{
	sprintf(ligne, "screen : %d x %d", LINES, COLS);
	mvaddstr(1, 0, ligne);
	}
standout();
for (foo=0; foo<COLS; foo++)	mvaddch(0, foo, '|');
mvaddstr(0, 0, title);
standend();
refresh();

return 0;		/* this is not a magic number */
}
/* ----------------------------------------------------------------- */
int blast_error_message(char *txt, int violence, int unused)
{
int		line, foo;
char		buff[80];

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( '%s'  %d %d )\n", __func__,
					txt, violence, unused);
#endif 

line = LINES - 1;
if (verbosity > 3) {
	sprintf(buff, "%s : line is %d  ", __func__, line);
	mvaddstr(1, 0, buff);		refresh();
	}
	
standout();
// setup our part of screen
for (foo=0; foo<COLS; foo++) {
	mvaddch(line, foo, '|');
	}
mvaddstr(line,  0, "SYS$MSG_| ");
mvaddstr(line, 10, txt);
standend();
refresh();

return 0;
}
/* ----------------------------------------------------------------- */
int erase_error_message(int ascii)
{
int		foo;

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( '%c' )\n", __func__, ascii);
#endif 

standout();
// clear our part of screen
for (foo=0; foo<COLS; foo++) {
	mvaddch(LINES-1, foo, ascii);
	}
standend();	refresh();

return 0;
}
/* ----------------------------------------------------------------- */
/* warning: only use the bit 0 of the 'state' arg */

int draw_a_button(WINDOW *w, int lig, int col, char *txt, int state)
{
#if DEBUG_LEVEL > 1
fprintf(stderr, ">>> %s ( %3d %3d    '%s' %d )\n", __func__,
				lig, col, txt, state);
#endif

if (state) {
	standout();
	mvaddstr(lig,   col,   "*****");
	mvaddstr(lig+1, col,   "*");
	mvaddstr(lig+1, col+1, txt);
	mvaddstr(lig+1, col+4, "*");
	mvaddstr(lig+2, col,   "*****");
	standend();
	}
else	{
	mvaddstr(lig,   col,   "+---+");
	mvaddstr(lig+1, col,   "|   |");
	mvaddstr(lig+2, col,   "+---+");
	}
refresh();

return 0;
}
/* ----------------------------------------------------------------- */
/* ----------------------------------------------------------------- */
