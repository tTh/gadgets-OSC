/*
 *	senders.h    *   part of poc-osc
 */

/* ------------------------------------------------------- */

int	send_data_xy(lo_address dst, int x, int y);
int	send_data_zw(lo_address dst, int w, int z);
int	send_data_button(lo_address dst, int n, int v);
int	send_data_id(lo_address dst, char *s);

/* ------------------------------------------------------- */
/* ------------------------------------------------------- */

