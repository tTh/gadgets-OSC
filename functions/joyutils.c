/*
 *		joystick utility functions
 */
#include  <stdio.h>
#include  <stdlib.h>
#include  <string.h>
#include  <unistd.h>
#include  <fcntl.h>

#include  <linux/joystick.h>

#include  "joyutils.h"

/* ----------------------------------------------------------------- */
static char *type2txt(unsigned char type)
{
static char	temp[100];

if (!(type & JS_EVENT_INIT)) {
	switch(type) {
		case JS_EVENT_BUTTON:		return "button";
		case JS_EVENT_AXIS:		return "axis";
		}
	}
else	{
	sprintf(temp, "init %d", type & 0x7f);
	return temp;
	}
return "???";
}
/* ----------------------------------------------------------------- */
void dump_my_joystick(char *joy_device)
{
int			joy_fd, foo;
struct js_event 	js;
int			flag = 0;
unsigned long		debut;
char			joy_name[128];

if( ( joy_fd = open(joy_device , O_RDONLY)) == -1 ) {
	fprintf(stderr, "%s: couldn't open %s\n", __func__, joy_device);
	exit(1);
	}

if (ioctl(joy_fd, JSIOCGNAME(sizeof(joy_name)), joy_name) < 0)
	strncpy(joy_name, "Unknown", sizeof(joy_name));
fprintf(stderr, "Name: \"%s\"\n", joy_name);

for (;;) {
	foo = read(joy_fd, &js, sizeof(struct js_event));
	if (8 != foo) {
		fprintf(stderr, "%s: err reading joy\n", __func__);
		exit(1);
		}
	if ( ! flag ) {
		debut = js.time;
		flag = 1;
		}
	printf("%8lu     %4d      %-8s    %3d      %7d\n",
			js.time-debut,
			js.type, type2txt(js.type),
			js.number, js.value);
	}
}
/* ----------------------------------------------------------------- */

/* ----------------------------------------------------------------- */
