/*
 *		ncurse funcs
 */

#include  <curses.h>

int initcurses(void);
void endcurses(int p);

int draw_main_screen(char *title, int unused);
int blast_error_message(char *txt, int violence, int unused);
int erase_error_message(int ascii);

/* warning: only use the bit 0 of the 'state' arg */
int draw_a_button(WINDOW *w, int lig, int col, char *txt, int state);

/*
 *	bigchars.c
 */

int essai_bigchars(char *texte, int stand);
