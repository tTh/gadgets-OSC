#include  <stdio.h>
#include  <stdlib.h>
#include  <termios.h>		/* serial things */
#include  <unistd.h>
#include  <fcntl.h>

#include  "serial.h"

/* ----------------------------------------------------------------- */
/*
 *	this variable must be declared/initialised in your
 *	main program.
 */
 
extern int verbosity;

/* ----------------------------------------------------------------- */
/* values for 'bauds' can be found in /usr/include/bits/termios.h
 */
int open_serial(char *dev, int bauds, int unused)
{
int		fd;

#if DEBUG_LEVEL
fprintf(stderr, "opening %s (b/s code : 0%06o)\n", dev, bauds);
#endif

fd = open(dev, O_RDWR, O_NOCTTY);
if (fd < 0) {
	perror(dev);
	return -1;
	}

return fd;
}
/* ----------------------------------------------------------------- */
/*
 *		from the manpage
 The  zero baud rate, B0, is used to terminate the connection. If B0 is
 specified, the modem control lines shall no longer be asserted. Normally,  
 this will disconnect the line.  
 */

int bauds_to_B(int bauds)
{

switch (bauds) {
	case	50:		return B50;
	case	75:		return B75;
	case	110:		return B110;
	case	300:		return B300;
	case	1200:		return B1200;
	case	2400:		return B2400;
	case	4800:		return B4800;
	case	9600:		return B9600;
	case	19200:		return B19200;
	}

return B0;
}
/* ----------------------------------------------------------------- */
/* ----------------------------------------------------------------- */
/* ----------------------------------------------------------------- */
