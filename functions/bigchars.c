/*
   !	ncurses widgets for poc-osc
   !    
   !           nnnnn      n     nnnn
   !           n    n     n    n    n
   !           nnnnn      n    n
   !           n    n     n    n  nnn
   !           n    n     n    n    n
   !           nnnnn      n     nnnn
   !    
   !    
   !      nnnn   n    n    nn    nnnnn    nnnn
   !     n    n  n    n   n  n   n    n  n
   !     n       nnnnnn  n    n  n    n   nnnn
   !     n       n    n  nnnnnn  nnnnn        n
   !     n    n  n    n  n    n  n   n   n    n
   !      nnnn   n    n  n    n  n    n   nnnn
   !    
*/
 
#include  <stdio.h>
#include  <stdlib.h>
#include  <curses.h>
#include  <signal.h>
#include  <locale.h>

#include  "ncursefuncs.h"

/* XXX */
#include  "chars8x8.def"

extern int verbosity;		/* to be declared public near main() */

/* ----------------------------------------------------------------- */
/* ----------------------------------------------------------------- */

int essai_bigchars(char *texte, int stand)
{

fprintf(stderr, ">>> %s ( '%s' %d )\n", __func__, texte, stand);

return -1;
}
/* ----------------------------------------------------------------- */
