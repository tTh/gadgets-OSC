/*
 *   Pour compiler :
 *     gcc -Wall osc-joy.c -llo -o osc-joy
 *
 *   Options :
 *        -j joydevice			default /dev/input/js0
 *        -h destination (or IP addr)	default localhost 
 *        -p destport			default is 9000
 *        -o nnn			offset for button number
 */  


#include  <fcntl.h>
#include  <stdio.h>
#include  <stdlib.h>
#include  <unistd.h>
#include  <string.h>
#include  <sys/ioctl.h>

#include <linux/joystick.h>
#include <lo/lo.h>

#include  "functions/senders.h"
#include  "functions/joyutils.h"

/*   default values, can be changed on command line */

#define REMOTE_HOST	"localhost"		/* just loling */
#define REMOTE_PORT	"9000"
#define	JOY_DEVICE	"/dev/input/js0"
#define MY_TEXT_ID	"suck my stick"

/* ----------------------------------------------------------------- */
int		verbosity = 0;
int		button_offset = 0;

char		*my_id = MY_TEXT_ID;

/* ----------------------------------------------------------------- */
static void help(int k)
{
puts("\t * joystick -> osc       "__DATE__" *");
puts("\t-D\tdump joystick datas");
puts("\t-r\tremote host ("REMOTE_HOST")");
puts("\t-p\tremote UDP port ("REMOTE_PORT")");
puts("\t-j\tjoystick device ("JOY_DEVICE")");
puts("\t-v\tincrease verbosity");
printf("\t-o\toffset added to button number (%d)\n", button_offset);
puts("\t-I\tchange text id (\""MY_TEXT_ID"\")");
exit(0);
}
/* ----------------------------------------------------------------- */
int main(int argc, char *argv[])
{
int		foo, joy_fd;
struct js_event js;
int		x_pos, y_pos, z_pos, w_pos, what;
char		joy_name[128];
int		opt;
char		*remote_host = REMOTE_HOST;
char		*remote_port = REMOTE_PORT;
char		*joy_device = JOY_DEVICE;
int		do_dump = 0;

/* parsing command line options */
while ((opt = getopt(argc, argv, "Dhp:r:vj:o:I:")) != -1) {
	switch (opt) {
		case 'D':	do_dump = 1;		break;
		case 'h':	help(0);		break;
		case 'r':	remote_host = optarg;	break;
		case 'p':	remote_port = optarg;	break;
		case 'j':	joy_device = optarg;	break;
		case 'v':	verbosity++;		break;
		case 'o':	button_offset = atoi(optarg);
				break;
		case 'I':	my_id = optarg;		break;
		default:	exit(1);		break;
		}
	}

if (do_dump)	{
	fprintf(stderr, "dumping data from '%s'\n", joy_device);
	dump_my_joystick(joy_device);
	}

if (verbosity) {
	fprintf(stderr, "  %s is sending to %s:%s\n", argv[0],
			remote_host, remote_port);
	fprintf(stderr, "  the stick '%s' is on %s\n", my_id, joy_device);
	} 

lo_address t =  lo_address_new(remote_host, remote_port);
/*
 *   XXX   no error check ? wtf ?
 */

if( ( joy_fd = open(joy_device , O_RDONLY)) == -1 ) {
	fprintf(stderr, "%s: Couldn't open %s\n", argv[0], joy_device);
	exit(1);
	}

if (verbosity) {
	if (ioctl(joy_fd, JSIOCGNAME(sizeof(joy_name)), joy_name) < 0)
		strncpy(joy_name, "Unknown", sizeof(joy_name));
	fprintf(stderr, "  Name: %s\n", joy_name);
	}

send_data_id(t, my_id);

x_pos = y_pos = z_pos = w_pos = 0;
for (;;) {
	foo = read(joy_fd, &js, sizeof(struct js_event));
	if (8 != foo) {
		fprintf(stderr, "err reading joy\n");
		exit(1);
		}
	/* calibration datas ignored */
	if (js.type > 128)	continue;

	if (verbosity>1) {	
		fprintf(stderr, "%10u  %2d  %2d %7d\n",
			js.time, js.type, js.number, js.value);
		}
	if (2==js.type)	{		/* oscillating stick */
		what = 0;
		switch (js.number) {
			case 0:
				x_pos = js.value;
				what = 1;
				break;
			case 1:
				y_pos = js.value;
				what = 1;
				break;
			case 2:
				z_pos = js.value;
				what = 2;
				break;
			case 3:
				w_pos = js.value;
				what = 2;
				break;
			}
		/* now, send the datas */
		// fprintf(stderr, "\t\twhat = %d\n", what);
		switch (what) {
			case 1:
				foo = send_data_xy(t, x_pos, y_pos);
				what = 0;
				break;
			case 2:
				foo = send_data_zw(t, z_pos, w_pos);
				what = 0;
				break;
				}
			}

	if (1==js.type)	{		/* it's a button */
		foo = send_data_button(t, js.number+button_offset,
						 js.value);
		}
	}
	
return 0;
}
/* ----------------------------------------------------------------- */
