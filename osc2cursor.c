/*
 *   dessiner avec OSC et la manette de jeu 
 */
 
#include  <stdio.h>
#include  <stdlib.h>
#include  <unistd.h>
#include  <limits.h>		/* for SHRT_MIN */
#include  <time.h>
#include  <curses.h>
#include  <signal.h>
#include  <locale.h>

#include  <getopt.h>
#include  <lo/lo.h>

#include  "functions/ncursefuncs.h"

/* ----------------------------------------------------------------- */

#define	LOCAL_PORT	"9000"

int		verbosity = 0;
unsigned long	hits = 0L;
int		erase_button = -1;
int		current_char = '%';
volatile int	must_erase = 0;

/* ----------------------------------------------------------------- */

void error(int num, const char *msg, const char *path)
{
fprintf(stderr, "liblo server error %d in path %s : %s\n", num, path, msg);
exit(1);
}
/* ----------------------------------------------------------------- */
int button_handler(const char *path, const char *types, lo_arg ** argv,
                int argc, void *data, void *user_data)
{
char		ligne[80];

#if DEBUG_LEVEL > 1
printf(stderr, "%s : %s %s %d\n", __func__, path, types, argc);
#endif

if (verbosity) {
	sprintf(ligne, "%s %s %s %d\n", __func__, path, types, argc);	
	mvaddstr(2, 2, ligne);
	sprintf(ligne, "button : %6d %6d", argv[0]->i, argv[1]->i);
	mvaddstr(3, 2, ligne);
	refresh();
	}

if (-1 == erase_button)		return 0;

if (	(argv[0]->i == erase_button)	&&
	(argv[1]->i == 0)		) {
	must_erase = 1;
	mvaddstr(3, 2, "KILL ! KILL !");
	refresh();
	}

return 0;
}

/* ----------------------------------------------------------------- */

static int	old_c = 0, old_l = 0;

int xy_handler(const char *path, const char *types, lo_arg ** argv,
                int argc, void *data, void *user_data)
{
char		ligne[80];
int		val_x, val_y;
int		cur_l, cur_c;
float		kx, ky;

if (verbosity>1) fprintf(stderr, "%s : %s %s %d\n", __func__,
					path, types, argc);

if (must_erase)	{
	move(1, 0);
	clrtobot();
	must_erase = 0;
	}

standout();
val_x = argv[0]->i;	val_y = argv[1]->i;

if (verbosity)	{
	sprintf(ligne, " xy %6d %6d ", val_x, val_y);
	mvaddstr(0, 30, ligne);
	hits++;
	sprintf(ligne, " %7lu ", hits);	
	mvaddstr(0, COLS-12, ligne);
	}
standend();


/* boundary checking */
if (val_x < SHRT_MIN)		val_x = SHRT_MIN;
if (val_y < SHRT_MIN)		val_y = SHRT_MIN;
if (val_x > SHRT_MAX)		val_x = SHRT_MAX;
if (val_y > SHRT_MAX)		val_y = SHRT_MAX;

/* make values positives */
val_x += -SHRT_MIN;
val_y += -SHRT_MIN;

/* compute position of the new spot */
kx = (float)COLS / 65535.0;
ky = (float)(LINES-1) / 65535.0;

cur_c =     (int)((float)val_x * kx);
cur_l = 1 + (int)((float)val_y * ky);

if (verbosity) {	
	fprintf(stderr, "%7d %7d  ->  %7d %7d\n",
				val_x, val_y, cur_l, cur_c);
	}
	
/* erase the old spot */
if (old_l || old_c)	mvaddch(old_l, old_c, current_char);

standout();
mvaddch(cur_l, cur_c, '#');
standend();

old_l = cur_l, old_c = cur_c;

refresh();

return 0;
}
/* ----------------------------------------------------------------- */
/* ----------------------------------------------------------------- */
static int help(int notused)
{
fprintf(stderr, "\t * osc2cursor      %s\n", __DATE__" *");

puts("\t-p\tlistening UDP port ("LOCAL_PORT")");
puts("\t-v\tincrease verbosity");
puts("\t-E\terasing button number (0)");
puts("\t-C\tdrawing character (NA)");

return -1;
}
/* ----------------------------------------------------------------- */

int main(int argc, char *argv[])
{
int		foo;
lo_server_thread st;
char		*local_port = LOCAL_PORT;
int		opt;
char		ligne[81];

/* parsing command line options */
while ((opt = getopt(argc, argv, "hp:vE:C:")) != -1) {
	switch (opt) {
		case 'h':	if (help(0)) exit(1);	break;
		case 'p':	local_port = optarg;	break;
		case 'v':	verbosity++;		break;
		case 'E':	erase_button = atoi(optarg);
				break;
		case 'C':	current_char = *optarg;	break;
		default:	exit(1);
		}
	}

fprintf(stderr, "erase %d\n", erase_button);

st = lo_server_thread_new(local_port, error);

foo = initcurses();
sprintf(ligne, ":%s  e=%d ", local_port, erase_button);
foo = draw_main_screen(ligne, 0);
// if (verbosity) fprintf(stderr, "dms %d\n", foo);

lo_server_thread_add_method(st, "/joystick/xy", "ii", xy_handler, NULL);
lo_server_thread_add_method(st, "/joystick/b", "ii", button_handler, NULL);

lo_server_thread_start(st);

for (;;) {
	if (verbosity)
		fprintf(stderr, "t = %ld\n", time(NULL));
	sleep(100);
	}

return 0;
}
/* ----------------------------------------------------------------- */
