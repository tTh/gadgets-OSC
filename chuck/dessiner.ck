
/*
 *		dessiner par OSC
 *		(avec le laser de Gaby ?)
 *
 *	nouveau Rural Art System 2021 - TerreBlanque 
 *
 */

9000		=> int OSCPort;
32760.0		=> float Amplitude;

OscOut xmit;
float	dx, dy;

xmit.dest("localhost", OSCPort);

clear_the_screen();

for (0 => int foo; foo<25800; foo++) {
	<<< foo >>>;

	Amplitude * Math.sin(foo * 0.3001) => dx;
	Amplitude * Math.cos(foo * 0.4999) => dy;

	xmit.start("/joystick/xy");
	Std.ftoi(dx) => xmit.add;
	Std.ftoi(dy) => xmit.add;
	xmit.send();

	0.025::second => now;
	}

//   ----------------------------------------------------

fun void clear_the_screen()
{
xmit.start("/joystick/b");
50 => xmit.add;	1 => xmit.add;
xmit.send();
xmit.start("/joystick/b");
50 => xmit.add;	0 => xmit.add;
xmit.send();
}

//   ----------------------------------------------------
