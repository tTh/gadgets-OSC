/*
 *	recevoir des notes on/off par le MIDI
 *	et envoyer des boutons de joy par OSC
 *
 *	nouveau Rural Art System 2021 - TerreBlanque 
*/

MidiIn		input;
MidiMsg		msg;
OscOut 		xmit;

1 => int port;			// please explain
9000 => int OSCPort;

if (!input.open(port)) {
	<<< "erreur open" >>>;
	me.exit();
	}

// print out device that was opened
<<< "MIDI device:", input.num(), " -> ", input.name() >>>;

xmit.dest("localhost", OSCPort);


// infinite loop
while (1)	{
	input => now;

	while ( input.recv(msg) ) {
		<<< msg.data1, msg.data2, msg.data3 >>>;

		xmit.start("/joystick/b");



		}



	}


