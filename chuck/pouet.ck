/*
   faire pouet avec chuck osc et un joystick
   -----------------------------------------
		reception des boutons
*/

7778 => int InPort;

SqrOsc sl 	=> Envelope envl =>	dac.left;
SawOsc sr 	=> Envelope envr =>	dac.right;
0.5		=> sl.gain => sr.gain;

0.22 => envl.time;	0.22 => envr.time;

OscIn	oscin;		OscMsg	msg;

InPort => oscin.port;	oscin.addAddress( "/joystick/b, ii" );

int button, value;	50 => int base;

<<< "listening on port", InPort >>>;

while( true ) {
	oscin => now;
	while ( oscin.recv(msg) ) { 
		msg.getInt(0) => button;
		msg.getInt(1) => value;
        	<<< "got (via ", InPort,")\tb", button, "\tv", value >>>;
		if (1 == value) {	/* button down */
			Std.mtof(base+(button))    => sl.freq;
			Std.mtof(base+12+(button)) => sr.freq;
			1 => envl.keyOn => envr.keyOn;
			}
		else	{
			1 => envl.keyOff => envr.keyOff;
			}
		0.1::second => now;
		}
	}
	
/* made in pi-ta-mere by tTh */

