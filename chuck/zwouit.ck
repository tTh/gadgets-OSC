/* 
   -----------------------------------------
		reception des XY
*/

7777		=>  int  InPort;
15.55		=>  float divisor;
SqrOsc sl 	=>  dac.left;
SawOsc sr 	=>  dac.right;
0.0		=>  sl.gain => sr.gain;

OscIn	oscin;
OscMsg	msg;
InPort => oscin.port;
oscin.addAddress( "/joystick/xy, ii" );

int x, y;
55 => int base;

0.5 => sl.gain => sr.gain;

<<< "listening on port", InPort >>>;

while( true ) {
	oscin => now;
	while ( oscin.recv(msg) ) { 
		msg.getInt(0) => x;
		msg.getInt(1) => y;
		<<< "got (via ", InPort,")  ", x, y >>>;
		x/12.0 => sl.freq;
		y/12.0 => sr.freq;
		}

	}
	
/* made in doubledragon2 by tTh */

/* --------------------------------------------------------
                      conversion coordonnées vers frequence */



