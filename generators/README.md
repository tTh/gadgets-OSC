# Générateurs

Pour la plupart de ces logiciels, le bouton 50 est utilisé
pour demander l'effacement d'un hypothétique écran.
Voir [osc-joy](../#osc-joy) pour les détails pratiques.

```
tth@fubar:~/Devel/gadgets-OSC/generators$ ./lissajous.pl 
Can't locate Net/OpenSoundControl/Client.pm in @INC
(you may need to install the Net::OpenSoundControl::Client module)
```

Oups, que faire ?


```
root@fubar:~# cpan
cpan[3]> install Net::OpenSoundControl
```

Et voilà...


## moresinus.pl

Options : `-d host:port` et `-v`

## lissajous

Options : `-d host:port` et `-v`

