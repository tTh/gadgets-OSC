#!/usr/bin/perl -w

my $port = 9001;
my $target = "localhost";

# ----------------------------------------------------------

use Net::OpenSoundControl::Client;
use Data::Dumper qw(Dumper);
use Getopt::Std;

my $client;
my $verbose = 0;

# ----------------------------------------------------------
#
#		MAIN
#
getopts('d:v', \%options);

if (defined $options{"d"}) {
	# print Dumper $options{"d"};
	($target, $port) = split /:/, $options{"d"};
	}
$verbose = 1 if (defined $options{"v"});

print "trashing ", $target, " on port ", $port, "\n";

$client =  Net::OpenSoundControl::Client->new(
		Name => "testkontrol",
		Host => $target, Port => $port)
		or die "Could not start client: $@\n";

print Dumper $client if $verbose;

my ($foo, $knum, $kval);

for ($foo=0; $foo<128; $foo++) {
	$kval = int(128*rand());
	$knum = int(128*rand());
	
	print "$knum -> $kval\n" if $verbose;
	$client->send(['/kontrol/v', 'i', $knum, 'i', $kval]);
	sleep(1);
	}

# ----------------------------------------------------------

