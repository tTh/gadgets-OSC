#!/usr/bin/perl -w

#
#		Ugly code by tTh
#


my $port = 9000;
my $target = "localhost";

# ----------------------------------------------------------

use Net::OpenSoundControl::Client;
use Data::Dumper qw(Dumper);
use Getopt::Std;

my $client;
my $verbose = 0;

# ----------------------------------------------------------

sub lissajous($$$)
{
my $kx = shift;		my $ky = shift;
my $phi = shift;

my ($i, $rads, $x, $y);
	
for ($i = 0; $i <= 360; $i++) {
	$rads = $i * (3.141592654/180);	
	$x = int(31250*sin(($rads*$kx)+$phi));
	$y = int(31250*cos($rads*$ky));
	# printf "%5d  %9f   %6d  %6d\n", $i, $rads, $x, $y;
	$client->send(['/joystick/xy', 'i', $x, 'i', $y]);
	# sleep 0.42;
	}
sleep 1+rand(2);
$client->send(['/joystick/b', 'i', 50, 'i', 1]);
$client->send(['/joystick/b', 'i', 50, 'i', 0]);
}

# ----------------------------------------------------------
#		main proggy

getopts('d:v', \%options);

if (defined $options{"d"}) {
	# print Dumper $options{"d"};
	($target, $port) = split /:/, $options{"d"};
	}
$verbose = 1 if (defined $options{"v"});
print "trashing ", $target, " on port ", $port, "\n";


$client =  Net::OpenSoundControl::Client->new(
		Host => $target, Port => $port)
		or die "Could not start client: $@\n";

print Dumper $client if $verbose;

my ($xk, $yk, $phase);

for ($xk=1; $xk<6; $xk++) {
	for ($yk=1; $yk<6; $yk++) {
		print "$$   ->   $xk  $yk\n";
		for ($phase=0.0; $phase<1.5705; $phase+=0.0911) {
			print "phi = ", $phase, "\n" if $verbose;
			lissajous($xk, $yk, $phase);
			}
		}
	}	 




