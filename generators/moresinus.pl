#!/usr/bin/perl -w


use Net::OpenSoundControl::Client;
use Data::Dumper qw(Dumper);
use Getopt::Std;

my $client;
my $verbose = 0;
my $dhost = "localhost";
my $dport = 9000;

# ----------------------------------------------------------

sub AskErase($)
{
my $b = shift;
$client->send(['/joystick/b', 'i', $b, 'i', 1]);
$client->send(['/joystick/b', 'i', $b, 'i', 0]);
}

# ----------------------------------------------------------
print "--- Moresinus $$ ---\n";

getopts('d:v', \%options);

if (defined $options{"d"}) {
	# print Dumper $options{"d"};
	($dhost, $dport) = split /:/, $options{"d"};
	}
$verbose = 1 if (defined $options{"v"});
print "trashing ", $dhost, " on port ", $dport, "\n";

$client =  Net::OpenSoundControl::Client->new(
		Host => $dhost, Port => $dport)
		or die "Could not start client: $@\n";

print Dumper $client if $verbose;

# ----------------------------------------------------------

AskErase(50);
srand($$);
print "   ", rand(1000), "\n";

my ($t, $v, $r1, $r2, $phi);

$r1 = rand(100) + 1600;
$r2 = rand(100) + 3200;
$phi = rand(3124) / 1000.0;
print "     $r1 $r2   $phi\n" if $verbose;

for ($t = -32000; $t < 32000; $t+=150) {

	$v = 15000 * (sin($t / $r1) + cos($phi + ($t / $r2)));

	# print $t, "   -->  ", $v, "\n";
	
	$client->send(['/joystick/xy', 'i', $t, 'i', $v]);

	sleep 1 if (($t % 16) == 0);	
	}

