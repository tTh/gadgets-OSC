/*
 *		Joystick to Laser
 *
 *		Sonoptic 2020
 */


#include  <stdio.h>
#include  <unistd.h>
#include  <stdlib.h>
#include  <getopt.h>
#include  <time.h>

#include  <lo/lo.h>			/* OSC library */

#include  "laserblast.h"

/* ------------------------------------------------------------------- */

#define	LOCAL_PORT	"9000"
#define REMOTE_HOST	"localhost"			/* just loling */
#define REMOTE_PORT	"9999"

int		verbosity = 0;

int		scenenumber = 1,
		lasernumber = 1;
/* ------------------------------------------------------------------- */
int draw_this_point(int x, int y)
{
float		fx, fy;

fx = (32768.0 + (float)x) / 94.7;
fy = (32768.0 + (float)y) / 94.7;

#define SZ 10.0

blast_rewind();
blast_addpoint(fx-SZ, fy-SZ, 0xffffff);
blast_addpoint(fx-SZ, fy+SZ, 0xffffff);
blast_addpoint(fx+SZ, fy+SZ, 0xffffff);
blast_addpoint(fx+SZ, fy-SZ, 0xffffff);
blast_addpoint(fx-SZ, fy-SZ, 0);

blast_flush(0);

return 0;
}
/* ------------------------------------------------------------------- */
int xy_handler(const char *path, const char *types, lo_arg ** argv,
                int argc, void *data, void *user_data)
{
int		val_x, val_y;
int		foo;

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( %s %s %d )\n", __func__, path, types, argc);
#endif

val_x = argv[0]->i;	val_y = argv[1]->i;

if (verbosity)		fprintf(stderr, "%6d  %6d\n", val_x, val_y);

foo = draw_this_point(val_x, val_y);

return 0;
}
/* ------------------------------------------------------------------- */
void error(int num, const char *msg, const char *path)
{
fprintf(stderr, "liblo server error %d in path %s : %s\n", num, path, msg);
exit(1);
}
/* ------------------------------------------------------------------- */
int help(int k)
{
puts("HELP ME !");

puts("\t-p NNN\t\tlocal listening port");
puts("\t-R a.b.c.d\tremote host");
puts("\t-P NNN\t\tremote port");
puts("\t-L N\t\tlaser number");
puts("\t-S N\t\tscene number");
puts("\t-v\t\tincrease verbosity");

return 1;
}
/* ------------------------------------------------------------------- */
int main(int argc, char *argv[])
{
char			*local_port  = LOCAL_PORT;
char			*remote_host = REMOTE_HOST;
char			*remote_port = REMOTE_PORT;

int			opt, foo;
lo_server_thread	st;
long			starttime;

fprintf(stderr, "%s compiled %s at %s\n",argv[0],  __DATE__, __TIME__);

/* parsing command line options */
while ((opt = getopt(argc, argv, "hp:vL:S:R:P:")) != -1) {
	switch (opt) {
		case 'h':	if (help(0)) exit(1);		break;
		case 'p':	local_port = optarg;		break;
		case 'R':	remote_host = optarg;		break;
		case 'P':	remote_port = optarg;		break;
		case 'L':	lasernumber = atoi(optarg);	break;
		case 'S':	scenenumber = atoi(optarg);	break;
		case 'v':	verbosity++;			break;
		}
	}

if (verbosity) {
	fprintf(stderr, "-----------: %s\n", argv[0]);
	fprintf(stderr, "pid        : %d\n", getpid());
	fprintf(stderr, "local port : %s\n", local_port);
	fprintf(stderr, "scene      : %d\n", scenenumber);
	fprintf(stderr, "laser      : %d\n", lasernumber);
	fprintf(stderr, "remote     : %s %s\n", remote_host, remote_port);
	}

foo = blast_init(remote_host, remote_port, scenenumber, lasernumber);
if (foo) {
	fprintf(stderr, "blast init error : %d\n", foo);
	exit(1);
	}

st = lo_server_thread_new(local_port, error);
lo_server_thread_add_method(st, "/joystick/xy", "ii", xy_handler, NULL);

lo_server_thread_start(st);

starttime = time(NULL) / 60;

for (;;) {
	if (verbosity)
		fprintf(stderr, "\tt = %ld min.\n",
					(time(NULL)/60L) - starttime);
	sleep(60);
	}

return 8;
}
/* ------------------------------------------------------------------- */