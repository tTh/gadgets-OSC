/*
 *		laserblsat over OSC
 *
 *		Sonoptic 2020
 */

#include  <stdio.h>
#include  <unistd.h>
#include  <stdlib.h>
#include  <string.h>

#include  <lo/lo.h>			/* OSC library */

/* ------------------------------------------------------------------- */

#define		BUFF_SIZE	8000

static char		big_buffer[BUFF_SIZE+2];
static int		curpos;
static int		la_scene, le_laser;
static lo_address	lo_addr;
static int		x_scale, y_scale;

extern int		verbosity;

/* ------------------------------------------------------------------- */
/*
 *	- prepare OSC transmission
 */
int	blast_init(char *host, char *port, int scene, int laser)
{
#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( '%s:%p' %d %d )\n", __func__, host, port,
							scene, laser);
#endif

lo_addr =  lo_address_new(host, port);
if (verbosity) {
	fprintf(stderr, "%s is sending to %s:%s\n", __FILE__, host, port);
	} 

la_scene = scene, le_laser = laser;
x_scale = y_scale = 700;

/* manage our internals memories */
big_buffer[0] = '\0';			/* clear buffer */
curpos = 0;				/* index in buffer */

return 0;
}
/* ------------------------------------------------------------------- */
/*
 *	this function is for test purpose, don't use in the
 *	real world, Just don't, OK?
 */
int blast_NOP(int reason)
{
int		foo;
char		msg[50];

fprintf(stderr, "===== BLAST NOP %d\n", reason);

sprintf(msg, "BLAST NOP %d", reason);
foo = lo_send(lo_addr, "/dev/null", "s", msg);
if (-1==foo) {
	fprintf(stderr, "%s : error %d\n", __func__, foo);
	return -1;
	}

return 0;
}
/* ------------------------------------------------------------------- */
int	blast_setscale(int width, int height)
{
#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( %d %d )\n", __func__, width, height);
#endif

/* no validity check ? */

x_scale = width;
y_scale = height;

return -1;
}
/* ------------------------------------------------------------------- */
int blast_rewind(void)
{

if (verbosity > 1)		fprintf(stderr, "Blast rewind\n");

big_buffer[0] = '\0';			/* clear buffer */
curpos = 0;				/* index in buffer */

strcat(big_buffer, "[");	curpos++;

return -1;
}
/* ------------------------------------------------------------------- */
int	blast_addpoint(float x, float y, long col)
{
char		buff[100];
int		sz;

sz = sprintf(buff, "(%.2f, %.2f, %ld),", x, y, col);

if (verbosity > 1) fprintf(stderr, "%s --> %s\n", __func__, buff);

/* check free space in bigbuffer */
if ( (curpos+sz) > BUFF_SIZE ) {
	fprintf(stderr, "buffer overflow in %s\n", __func__);
	exit(1);
	}

strcat(big_buffer, buff);		curpos += sz;

return -1;
}
/* ------------------------------------------------------------------- */
int	blast_flush(void)
{
int		foo;
char		buffer[100];

/* we have to kill the last ',' in the big buff before send */
big_buffer[curpos-1] = ']';	big_buffer[curpos] = '\0';

if (verbosity) fprintf(stderr, "BLAST  %s\n", big_buffer);

sprintf(buffer, "/pl/%d/%d", la_scene, le_laser);

foo = lo_send(lo_addr, buffer, "s", big_buffer);
if (-1==foo) {
	fprintf(stderr, "%s : error %d\n", __func__, foo);
	return -1;
	}

return 0;
}
/* ------------------------------------------------------------------- */
/* ------------------------------------------------------------------- */
/* ------------------------------------------------------------------- */

