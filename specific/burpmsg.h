/*
 *		BURPMSG.H
 */


/* ------------------------------------------------------------------- */

typedef struct	{
	uint32_t		magic;
	uint32_t		serial;
	uint16_t		msgtype;
	uint16_t		szdata;
	} BurpMsg;

#define		BURP_MAGIC	0x98769876

/*	message type		*/
#define		BURP_RESET	1
#define		BURP_SETSIZE	2
#define		BURP_LASER	3
#define		BURP_SCENE	4
#define		BURP_COLOR	5
#define		BURP_TEXTE	6

/* ------------------------------------------------------------------- */

int display_burp_msg(BurpMsg *header, void *data);

/* ------------------------------------------------------------------- */


