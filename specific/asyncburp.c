/*
 *		Joystick to Laser
 *		async version, with continuous refresh
 *				post Sonoptic 2020
 */

#include  <stdio.h>
#include  <stdlib.h>
#include  <unistd.h>
#include  <string.h>

#include  <lo/lo.h>			/* OSC library */

#include  "laserblast.h"

#include  "burpmsg.h"

/* ------------------------------------------------------------------- */

#define	LOCAL_PORT	"9000"
#define REMOTE_HOST	"localhost"			/* just loling */
#define REMOTE_PORT	"9999"

/* ------------------------------------------------------------------- */

int		verbosity;		/* global variable */

/* ------------------------------------------------------------------- */
/* ------------------------------------------------------------------- */
int init_osc_receiver(char *port, int notused)
{

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( '%s' %d )\n", __func__, port, notused);
#endif

return -1;
}
/* ------------------------------------------------------------------- */
int init_osc_sender(char *r_host, char *r_port, int notused)
{
int			foo;

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( '%s' '%s' %d )\n", __func__,
				r_host, r_port, notused);
#endif

foo = blast_init(r_host, r_port, 0, 0);
if (foo) {
	fprintf(stderr, "blast_init return %d\n", foo);
	return -2;
	}

if (verbosity) blast_NOP(getpid());

return 0;
}
/* ------------------------------------------------------------------- */
/*
 *	this is a tool function for debugging, do not use in real life
 */
static int tentative(void)
{
int		foo;

blast_rewind();

blast_addpoint(200, 200, 0xff0000);
blast_addpoint(300, 300, 0xff0000);

blast_flush(0);

return 42;
}
/* ------------------------------------------------------------------- */
static int help(int krkrkr)
{
puts("HELP ME !");
puts("\t-p NNN\t\tlocal listening port");
puts("\t-R a.b.c.d\tremote host");
puts("\t-P NNN\t\tremote port");
puts("\t-L N\t\tlaser number");
puts("\t-S N\t\tscene number");
puts("\t-v\t\tincrease verbosity");
return 1;
}
/* ------------------------------------------------------------------- */
int main(int argc, char *argv[])
{
char			*local_port  = LOCAL_PORT;
char			*remote_host = REMOTE_HOST;
char			*remote_port = REMOTE_PORT;

int			lasernumber, scenenumber;

int			opt, foo;

fprintf(stderr, "%s compiled %s at %s\n",argv[0],  __DATE__, __TIME__);

/* set some default values */

lasernumber = scenenumber = 0;

/* parsing command line options */
while ((opt = getopt(argc, argv, "hp:vL:S:R:P:")) != -1) {
	switch (opt) {
		case 'h':	if (help(0)) exit(1);		break;
		case 'p':	local_port = optarg;		break;
		case 'R':	remote_host = optarg;		break;
		case 'P':	remote_port = optarg;		break;
		case 'L':	lasernumber = atoi(optarg);	break;
		case 'S':	scenenumber = atoi(optarg);	break;
		case 'v':	verbosity++;			break;
		}
	}

if (verbosity) {
	fprintf(stderr, "-------------: %s\n", argv[0]);
	fprintf(stderr, "pid          : %d\n", getpid());
	fprintf(stderr, "local port   : %s\n", local_port);
	fprintf(stderr, "remote       : %s %s\n", remote_host, remote_port);
	fprintf(stderr, "scn/laser    : %d %d\n", scenenumber, lasernumber);
	}

foo = init_osc_sender(remote_host, remote_port, 0);
fprintf(stderr, "init osc sender -> %d\n", foo);

tentative();

return 2;
}
/* ------------------------------------------------------------------- */


