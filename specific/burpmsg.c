/*
 *		BURPMSG.C
 */

#include  <stdio.h>
#include  <stdint.h>
#include  <sys/select.h>
#include  <unistd.h>

#include  "laserblast.h"

#include  "burpmsg.h"

/* ------------------------------------------------------------------- */
int display_burp_msg(BurpMsg *header, void *data)
{
char			*txt_type;

if (BURP_MAGIC != header->magic) {
	fprintf(stderr, "burpmsg at %p : invalid magic %08X\n",
				header,		header->magic);
	return -1;
	}

txt_type = "wtf?";			/* nice default value */

switch(header->msgtype) {

	case BURP_RESET:
		txt_type = "reset";
		break;
	case BURP_SETSIZE:
		txt_type = "setsize";
		break;
	case BURP_LASER:
		txt_type = "laser";
		break;
	case BURP_SCENE:
		txt_type = "scene";
		break;
	case BURP_TEXTE:
		txt_type = "texte";
		break;

	default:
		fprintf(stderr, "burpmsg at %p : invalid msgtype %d\n",
				header,		header->msgtype);
	return -1;
	}

fprintf(stderr, "type %4d/%-8s  serial %9d  szdata %3d\n", 
		header->msgtype, txt_type, header->serial, header->szdata);

return 0;
}
/* ------------------------------------------------------------------- */
void la_grande_boucle( int fromfifo )
{
struct timeval		tv;
fd_set			fds;
int			resval, foo;
BurpMsg			message;

for(;;)	{			/* start of infinite loop */

	FD_ZERO(&fds);
	FD_SET(fromfifo, &fds);
	tv.tv_sec  = 1;
	tv.tv_usec = 0;

	resval = select(1, &fds, NULL, NULL, &tv);

	if (-1 == resval) {
		perror("select()");
		/* ABORTING HERE ? */
		continue;
		}

	if (resval) {
		/* message available */
		foo = read(fromfifo, &message, sizeof(BurpMsg));
		if (sizeof(BurpMsg) != foo) {
			fprintf(stderr, "err: %d read, %lu expected\n",
						foo,	sizeof(BurpMsg));
			}
		display_burp_msg(&message, NULL);
		/* read appended datas */

		}
	else	{
		/* got a time out */
		fprintf(stderr, "TIMEOUT\n");
		}

	}

/* NOT REACHED */

}
/* ------------------------------------------------------------------- */

