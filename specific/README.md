# Du joystick au laser

## joy2laser

Kluge intuité pendant [Sonoptic 2020](http://sonoptic.net/).

```
tth@debian:~/Devel/gadgets-OSC/specific$ ./joy2laser -h
HELP ME !
        -p NNN          local listening port
        -R a.b.c.d      remote host
        -P NNN          remote port
        -L N            laser number
        -S N            scene number
        -v              increase verbosity
```

En réception, c'est fait pour fonctionner avec des choses comme 
[osc-joy](../osc-joy.c) ou un [générateur](../generators/) de sinusoïde.
Le [protocole](../functions/senders.c) utilisé est simple
(parfois un peu trop) mais fonctionne bien.

En émission, ça tente de se conformer au
[protocole](https://git.interhacker.space/teamlaser/LJ/src/branch/master/README.md#lj-commands-reference)
défini par la teamlaser. Hélas, le soft de Sam n'étant pas très stable, ce logiciel.
n'a pas été vraiment testé sur le système réel.


```
<tth> vos lasers, la déviation elle se fait à quelle fréquence maxi ?
<sammmm> plop
<sammmm> voila une reponse si j'ai bien compris la question.
<sammmm> Le plus rapide de nos lasers peut tracer 30 000 pts /sec avec un angle max de 8°
<sammmm> Apres si tu traces que 2 points plus proches que 8° le nombre d'oscillations va etre plus elevé evidement puisqu'il faudra moins de tps pour aller au pts suivant puisqu'il est plus pres.
<sammmm> en resumé la frequence maxi est inversement proportionnelle a la deviation.
<sammmm> plus tu devies plus ca va etre long a tracer donc plus la frequence baisse.
```

