/*
 *				laserblast.h
 *
 *		sonoptic 2020
 */

int	blast_init(char *host, char *port, int scene, int laser);
int	blast_rewind(void);
int	blast_setscale(int width, int height);

int	blast_addpoint(float fx, float fy, long col);
int	blast_flush(int notused);

int	blast_NOP(int reason);
