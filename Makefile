# --------------------------------------------------------------

# umpf...

OPTS = -Wall -g -DDEBUG_LEVEL=0
DEPS = Makefile functions/senders.h functions/joyutils.h	\
	functions/ncursefuncs.h

all:	osc-joy osc2cursor text2osc showbuttons

# --------------------------------------------------------------

osc2cursor:        osc2cursor.c ${DEPS} functions/libpocosc.a 
	gcc ${OPTS} $< functions/libpocosc.a -llo -lcurses -o $@

osc-joy:        osc-joy.c ${DEPS} functions/libpocosc.a 
	gcc ${OPTS} $< functions/libpocosc.a -llo -o $@

text2osc:        text2osc.c ${DEPS} functions/libpocosc.a
	gcc ${OPTS} $< functions/libpocosc.a -llo -o $@

# --------------------------------------------------------------

showbuttons:        showbuttons.c functions/libpocosc.a ${DEPS}
	gcc ${OPTS} $< functions/libpocosc.a -llo -lcurses -o $@

# --------------------------------------------------------------
