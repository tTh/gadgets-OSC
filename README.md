# Gadgets autour du protocole OSC

Oui, je sais, tout ça n'est pas vraiment clair. Mais je me soigne.

Première étape : consulter le
[site d'OSC](https://opensoundcontrol.stanford.edu/) pour comprendre comment
ça fonctionne.

## prérequis

Première étape avant de générer les binaires, installer quelques
bibliothèques de support :

```
apt install liblo-tools liblo-dev
apt install libasound2-dev
apt install ncurses-dev
```
Plus le module Perl `Net::OpenSoundControl` à chercher dans le CPAN.

Ensuite, il faut compiler quelques fonctions utilisées par plusieurs
exemples :

```
cd functions
make
```

Et ensuite `make` dans le répertoire de base, je pense que c'est
assez simple, et parfois ça marche...

# Les programmes

## osc-joy

Lecture d'une manette de jeu USB et envoi des coordonnées x/y/z/w
et des boutons vers un écouteur OSC.

L'option `-o NN` rajoute NN au numéro de bouton.
Voir les [générateurs](generators/) pour les détails.

## osc2cursor

Une appli ncurses trop choupie :)

## text2osc

Pour faire __beep__ vers Chuck...

## showbuttons

Presque fini depuis quelques mois/années.






