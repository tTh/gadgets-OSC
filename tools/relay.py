#!/usr/bin/python3

import socket, sys, getopt

# --- default values

RX_UDP_IP = ""			# pour ecouter sur toutes les interfaces
RX_UDP_PORT = 5005		# entree du relais

# ---

rx_port = RX_UDP_PORT
verbose = 0

# --- parse command line arguments

options = "hvp:c:"
arguments = sys.argv[1:]
opts, args = getopt.getopt(arguments, options)

for o, a in opts:
    print ('    ', o, ' --> ', a)
    if		"-v" == o:	verbose += 1
    elif	"-p" == o:	rx_port = int(a)

print ("listening port    : ", rx_port)

# ---

cibles = [ ];
for ligne in open("destinations.liste"):
    a, p = ligne.replace('\n', '').split(":")
    # print a, p
    if p:	cibles.append((a, int(p)))

for cible in cibles:
    print ("   -> ", cible)
    

#   point d'entree, d'ecoute
sock_rx = socket.socket(socket.AF_INET, # Internet
                     socket.SOCK_DGRAM) # UDP
sock_rx.bind((RX_UDP_IP, rx_port))

#   point de sortie vers les autres
sock_tx = socket.socket(socket.AF_INET, # Internet
                     socket.SOCK_DGRAM) # UDP

while True:
    data, addr = sock_rx.recvfrom(1024) # buffer size is 1024 bytes
    if verbose:
        print ("got:", addr, " ", len(data))
    for cible in cibles:
        # print cible
        sock_tx.sendto(data, cible)
	
# hop, ce truc doit fonctionner !



