/*
 *		wait for joystick event
 *		-----------------------
 *
 #	https://git.tetalab.org/tTh/gadgets-OSC
 *
 */

#include  <stdio.h>
#include  <stdlib.h>
#include  <unistd.h>

int	verbosity = 0;

/* ----------------------------------------------------------------- */
/* ----------------------------------------------------------------- */
void help(int k)
{
puts("XXX");
exit(0);
}
/* ----------------------------------------------------------------- */
int main(int argc, char *argv[])
{
int		foo, opt;
char		*device = "/dev/input/js0";

/* parsing command line options */
while ((opt = getopt(argc, argv, "d:hv")) != -1) {
	switch (opt) {
		case 'd':	device = optarg;	break;
		case 'h':	help(0);		break;
		case 'v':	verbosity++;		break;
		default:	exit(1);
		}
	}

if (verbosity) {
	fprintf(stderr, "%s on %s\n", argv[0], device);
	}

return 0;
}
/* ----------------------------------------------------------------- */
