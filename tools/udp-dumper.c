/*
 *	general purpose UDP dumper
 *
 *	made by tTh, around 2019...
 */
#include  <stdio.h>
#include  <stdlib.h>
#include  <string.h>
#include  <unistd.h>
#include  <ctype.h>
#include  <sys/time.h>
#include  <arpa/inet.h>
#include  <sys/types.h>
#include  <sys/socket.h>

#define    PORT		5002
#define	   BUFLEN	1024

int	verbosity = 0;

/* ----------------------------------------------------------------- */
int dumpln(unsigned char *ptr)
{
int		foo;

for (foo=0; foo<16; foo++)
	printf("%02x ", ptr[foo]);

printf("| ");
for (foo=0; foo<16; foo++)
	if (isprint(ptr[foo]))	putchar(ptr[foo]);
	else			putchar('.');

printf(" |\n");
return 0;
}
/* ----------------------------------------------------------------- */
void udp_dumper(uint16_t port)
{
unsigned char		buffer[BUFLEN];
struct sockaddr_in	si_me, si_other;
int			sock, foo, bar, flag_exit;
unsigned int		slen=sizeof(si_other);
long			serial;
double			starttime, curtime;
struct timeval		tp;

fprintf(stderr, "%s: listen on port %d\n", __func__, port);

if (-1==(sock=socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP))) {
	perror("socket fail ");
	exit(1);
	}

#if DEBUG_LEVEL
fprintf(stderr, "port %d / sock -> %d\n", port, sock);
#endif

memset((char *) &si_me, 0, sizeof(si_me));
si_me.sin_family = AF_INET;
si_me.sin_port = htons(port);
si_me.sin_addr.s_addr = htonl(INADDR_ANY);
if (bind(sock, (struct sockaddr *)&si_me, sizeof(si_me))==-1) {
 	perror("'bind' failure ");
 	exit(1);
	}

flag_exit = 0;		serial = 0L;

gettimeofday(&tp, NULL);
starttime = tp.tv_sec + tp.tv_usec / 1e6;

do	{
	memset(buffer, 0, BUFLEN);
	slen = sizeof(si_other);
	foo = recvfrom(sock, buffer, BUFLEN, 0,
				(struct sockaddr *)&si_other, &slen);

	if (verbosity) {
		gettimeofday(&tp, NULL);
		curtime = tp.tv_sec + tp.tv_usec / 1e6;
		printf("----+ frame %4ld %5d bytes    t= %.3f sec.\n",
				serial, foo, curtime - starttime);
		/*
		 * MUST display ip:port of the sender !
		 */
		}
		
	for (bar=0; bar<=foo; bar+=16) {
		printf("%3d | ", bar);
		dumpln(buffer+bar);
		} 
	serial++;		
	} while ( !flag_exit);
}
/* ----------------------------------------------------------------- */
void help(int foo)
{
puts("\t-h\tthis help text");
puts("\t-p NNNN\treceiving port number");
puts("\t-v\tincrease verbosity");
exit(0);
return ;
}
/* ----------------------------------------------------------------- */
int main(int argc, char *argv[])
{
int		opt, port;

port = PORT;
/* parsing command line options */
while ((opt = getopt(argc, argv, "hp:v")) != -1) {
	switch (opt) {
		case 'h':	help(0);		break;
		case 'p':	port = atoi(optarg);	break;
		case 'v':	verbosity++;		break;
		default:	exit(1);
		}
	}


udp_dumper(port);

return 0;
}
/* ----------------------------------------------------------------- */

