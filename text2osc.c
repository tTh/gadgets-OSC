/*
 *		WARNING, THIS IS JUST A WIP !
 */

#include  <stdio.h>
#include  <stdlib.h>
#include  <unistd.h>
#include  <ctype.h>

#include  <lo/lo.h>
#include  "functions/senders.h"

#define REMOTE_HOST	"localhost"			/* just loling */
#define REMOTE_PORT	"9000"
#define MY_TEXT_ID	"<@TROLL>"
/* ----------------------------------------------------------------- */

int		verbosity = 0;
char		*my_id = MY_TEXT_ID;
int		wait_time = 80;		/* in milliseconds */

/* ----------------------------------------------------------------- */
int megaloop(FILE *fp, lo_address loa)
{
int		caractere, note;
short		x_val, y_val;
int		must_send_xy;
int		char_count;

x_val = y_val = 0;
must_send_xy = 1;
char_count = 0;

rewind(fp);		/* so we can run this code again and again
			   on the same fopened file */

while (EOF != (caractere=getc(fp))) {

	char_count++;
	
	if (verbosity) fprintf(stderr, "char = %4d   %c\n",
				caractere, caractere);
	
	if (isalpha(caractere)) {
		/* Play a sound */
		note=toupper(caractere) - 'A';
		// fprintf(stderr, "%c -> %3d\n", caractere, note);
		send_data_button(loa, note, 1);
		usleep(wait_time*1000);
		send_data_button(loa, note, 0);
		usleep(40*1000);
		}
	else if (isdigit(caractere)) {
		note = caractere - '0';
		send_data_button(loa, note, 1);
		usleep(wait_time*1000);
		send_data_button(loa, note, 0);
		usleep(20*1000);
		}

	else if (isblank(caractere)) {
		usleep(200*1000);
		}
	else if (caractere == '!') {
		send_data_id(loa, my_id);
		}

	/* imagine : we are a XY joystick */
	else if (caractere == '+') {
		x_val++;
		if (x_val > 32000)	x_val = 0;
		must_send_xy = 1;		
		}
	else if (caractere == '-') {
		x_val--;
		if (x_val < -32000)	x_val = 0;
		must_send_xy = 1;		
		}
	else if (caractere == '^') {
		y_val++;
		if (y_val > 32000)	y_val = 0;
		must_send_xy = 1;		
		}
	else if (caractere == '$') {
		y_val--;
		if (y_val < -32000)	y_val = 0;
		must_send_xy = 1;		
		}
				
	/* default case */
	else 	{	
		usleep(wait_time*1000);
		}
	
	if (must_send_xy) {
		fprintf(stderr, "...     X %6d   Y %6d\n",
					x_val, y_val);
		send_data_xy(loa, x_val, y_val);		
		must_send_xy = 0;
		}
	}

return 0;
}
/* ----------------------------------------------------------------- */
static void help(int k)
{
puts("\t * text -> osc       "__DATE__" *");
puts("\t-r\tremote host ("REMOTE_HOST")");
puts("\t-p\tremote UDP port ("REMOTE_PORT")");
puts("\t-v\tincrease verbosity");
puts("\t-w\twait time unit in ms.");
puts("\t-I\tchange text id (\""MY_TEXT_ID"\")");
exit(0);
}
/* ----------------------------------------------------------------- */

int main(int argc, char *argv[])
{
int		opt;
char		*remote_host = REMOTE_HOST;
char		*remote_port = REMOTE_PORT;
lo_address	loaddr;
FILE		*fp;

/* parsing command line options */
while ((opt = getopt(argc, argv, "hp:r:vw:I:")) != -1) {
	switch (opt) {
		case 'h':	help(0);		break;
		case 'r':	remote_host = optarg;	break;
		case 'p':	remote_port = optarg;	break;
		case 'v':	verbosity++;		break;
		case 'w':	wait_time=atoi(optarg);	break;
		case 'I':	my_id = optarg;		break;
		default:	exit(1);
		}
	}

if (optind >= argc) {
	fprintf(stderr, "Expected a filename after options\n");
	exit(EXIT_FAILURE);
	}
	
loaddr =  lo_address_new(remote_host, remote_port);
if (verbosity) {
	fprintf(stderr, "%s (%s) is sending to %s:%s\n",
			argv[0], my_id, remote_host, remote_port);
	} 

if (NULL==(fp=fopen(argv[optind], "r"))) {
	perror (argv[optind]);
	exit(1);
	}

if (verbosity) fprintf(stderr, "now playing %s\n", argv[optind]);

megaloop(fp, loaddr);


return 0;
}
/* ----------------------------------------------------------------- */
