# Le laser de Gaby

Une nouvelle aventure se prépare à Valensole, en voici l'histoire...

## Caractéristiques

Encore très flou, mais où donc est rangée cette doc ?

- position X/Y par deux valeurs analogiques
- niveau RGB par trois signaux PWM

https://en.wikipedia.org/wiki/International_Laser_Display_Association


## Logiciels

Deux composantes : le contrôleur "physique" du laser tournera dans
un Arduino Mega, et sera lui même piloté par un "récepteur" OSC dans
la machine hote. Ces deux parties vont communiquer par le classique
lien série/usb avec un protocole encore à définir.

### Coté hote/OSC

Dans un premier temps, je vais reprendre mon protocole utilisé pour
les joysticks, d'abord pour le positionnement X/Y, et ensuite pour
la gestion du RGB.

### Coté Arduino

Les choses sont moins claires, car j'ignore encore certaines choses comme
la qualité des sortie analogiques de l'Arduino. Il sera peut-être
nécessaire de conditionner les deux valeurs (intégration, gain, offset)
par un matériel approprié.

D'autre part, la fréquence de rafraichissement sera-elle suffisante ?

Et pour finir y aura-t-il assez de mémoire RAM pour stocker des dessins
de taille conséquente ?


## Conclusion

Il faut maintenant envoyer le Gobeti :)

