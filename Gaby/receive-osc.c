/*
 *				LE LASER DE GABY
 *
 *		+--------------------------------------------------+
 *		|  reception des trames osc depuis le grand monde  |
 *		+--------------------------------------------------+
 */

#include  <stdio.h>

#include  <lo/lo.h>

#include  "receive-osc.h"
#include  "transmit.h"

/* ---------------------------------------------------------------- */

static int	old_x, old_y;

int xy_handler(const char *path, const char *types, lo_arg ** argv,
                int argc, void *data, void *user_data)
{
int		val_x, val_y;
int		foo;

// fprintf(stderr, ">>> %s ( '%s' '%s' )\n", __func__, path, types);

val_x = argv[0]->i;	val_y = argv[1]->i;

fprintf(stderr, "osc ->    %7d   %7d\n", val_x, val_y);

if (old_x != val_x) {
	foo = send_position('X', val_x);
	old_x = val_x;
	}
if (old_y != val_y) {
	foo = send_position('Y', val_y);
	old_y = val_y;
	}

return -1;
}
/* ---------------------------------------------------------------- */
int button_handler(const char *path, const char *types, lo_arg ** argv,
                int argc, void *data, void *user_data)
{
int		foo;

fprintf(stderr, ">>> %s ( '%s' '%s' )\n", __func__, path, types);

foo = send_button(argv[0]->i, argv[1]->i);


return -1;
}
/* ---------------------------------------------------------------- */
