# Le protocole

Le lien série-sur-usb de l'arduino est parfois capricieux et souvent
plein de mystères...

Ayant de gros doutes sur sa capacité à transmettre des données binaires,
le choix d'un codage ASCII semble évident.

D'un autre coté, le débit du lien est assez faible, il faut compacter
le plus possible les données transférées. Un encodage type `base64`
est-il la bonne solution ?


