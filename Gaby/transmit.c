/*
 *				LE LASER DE GABY
 *
 *		+---------------------------------------------+
 *		|  transmission des commandes vers l'arduino  |
 *		+---------------------------------------------+

	$ oscsend localhost 9001 /joystick/xy ii 32000 -2210


 */

#include  <stdio.h>
#include  <stdlib.h>
#include  <unistd.h>
#include  <sys/types.h>
#include  <sys/stat.h>
#include  <fcntl.h>

#include  <termios.h>

#include  "../functions/serial.h"
#include  "transmit.h"

/* ---------------------------------------------------------------- */

static int		fdtx = -1;

/* ---------------------------------------------------------------- */
/*
 *	parametres :
 *	fname: a file for dumping data or a serial device
 *	bauds: speed of serial port, 0 for a file dump
 *
 */
int init_transmit(char *fname, int bauds)
{
int		fd;
int		baudsymb;

fprintf(stderr, ">>> %s ( '%s' %d )\n", __func__, fname, bauds);

if (0 == bauds) {
	/* open as a dump file */
	fd = open(fname, O_CREAT|O_WRONLY|O_TRUNC, S_IRWXU);
	if (fd < 0) {
		perror(fname);
		exit(1);
		}
	}
else	{

	/* open as as a serial out to Arduino */

	fprintf(stderr, "bauds are %d, exiting\n", bauds);
	exit(1);

	/*	XXX	*/

	}

fdtx = fd;
fprintf(stderr, "%s: fd is %d\n", __func__, fdtx);

return fd;
}
/* ---------------------------------------------------------------- */
/*
 *	ici on présupoose que la valeur est un "short int"
 *	donc 2^16 valeurs       -32768 32767
 */
int send_position(char xy, int value)
{
char		message[100];
char		val8;
int		foo;

if (fdtx < 0) {
	fprintf(stderr, "%s: fdtx not initialized !\n", __func__);
	exit(1);
	}

if (xy != 'X' && xy != 'Y') {
	fprintf(stderr, "%s: invalid xy tag '%c'\n", __func__, xy);
	return -1;
	}

val8 = (char)(value / 256);		/* -> signed byte */
sprintf(message, "%c%02x", xy, (unsigned char)val8);
fprintf(stderr, "  %c %8d %8d    %s\n", xy, value, val8, message);
foo = write(fdtx, message, 3);
if (3 != foo) {
	perror("write ?");
	return -1;
	}
return 0;
}

/* ---------------------------------------------------------------- */
/*
 *	comment gerer le up/down des boutons ?
 */

int send_button(int number, int state)
{
char		message[100];

if (fdtx < 0) {
	fprintf(stderr, "%s: fdtx not initialized !\n", __func__);
	exit(1);
	}

if (number<0 || number>16) {
	return -1;
	}

sprintf(message, "T%01x%1x", number, state);
fprintf(stderr, "%s ----> '%s'\n", __func__, message);
foo = write(fdtx, message, 3);
if (3 != foo) {
	perror("write ?");
	return -1;
	}
return 0;
}
/* ---------------------------------------------------------------- */
