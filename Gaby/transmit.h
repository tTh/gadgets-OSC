/*
 *				LE LASER DE GABY
 *
 *		+---------------------------------------------+
 *		|  transmission des commandes vers l'arduino  |
 *		+---------------------------------------------+
 */

int init_transmit(char *fname, int k);
int send_position(char xy, int value);

int send_button(int number, int state);

