/*
 *		LE LASER DE GABY
 */


#include  <stdio.h>
#include  <stdlib.h>
#include  <unistd.h>
#include  <time.h>

#include  <getopt.h>

#include  <lo/lo.h>

#include  "transmit.h"
#include  "receive-osc.h"

int		verbosity;

/* ---------------------------------------------------------------- */
static void error(int num, const char *msg, const char *path)
{
fprintf(stderr, "liblo server error %d in path %s : %s\n", num, path, msg);
exit(1);
}
/* ---------------------------------------------------------------- */
int init_rx_osc(char *port)
{
lo_server_thread	st;

st = lo_server_thread_new(port, error);

lo_server_thread_add_method(st, "/joystick/xy", "ii", xy_handler, NULL);
lo_server_thread_add_method(st, "/joystick/b", "ii", button_handler, NULL);

lo_server_thread_start(st);

fprintf(stderr, "%s [done]\n", __func__);

return 0;
}
/* ---------------------------------------------------------------- */
static void print_lo_version(char *bla)
{
#define SZ	100
char		str[SZ];
char		extra[SZ];

lo_version(str, SZ, 0, 0, extra, SZ, 0, 0, 0);

fprintf(stderr, "%s: liblo v%s %s\n", bla, str, extra);
}
/* ---------------------------------------------------------------- */
int help(int k)
{
fprintf(stderr, "help = %d\n", k);
return 1;
}
/* ---------------------------------------------------------------- */
int main(int argc, char *argv[])
{
char		*local_port = "9001";
int		bauds = 0;
int		foo, opt;

fprintf(stderr, "GabyLaser - compiled %s %s\n", __DATE__, __TIME__);

while ((opt = getopt(argc, argv, "hp:v")) != -1) {
	switch (opt) {
		case 'h':	if (help(0)) exit(1);	break;
		case 'p':	local_port = optarg;	break;
		case 'v':	verbosity++;		break;
		}
	}

if (verbosity) print_lo_version(argv[0]);

foo = init_rx_osc(local_port);
if (foo) {
	fprintf(stderr, "init rx osc -> %d\n", foo);
	exit(1);
	}

foo = init_transmit("./DUMP", bauds);
if (foo < 0) {
	fprintf(stderr, "init transmit -> %d\n", foo);
	exit(1);
	}

/*
 *	infinite loop is infinite
 */
for (;;) {
	if (verbosity)
		fprintf(stderr, "t = %ld\n", time(NULL));
	sleep(100);
	}


return 0;
}
/* ---------------------------------------------------------------- */
